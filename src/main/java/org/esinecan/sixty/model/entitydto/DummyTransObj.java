package org.esinecan.sixty.model.entitydto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * This is our DTO. It contains a minimal amount of validation.
 * Created by eren.sinecan on 20/04/2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class DummyTransObj {


    @NotNull
    private Long timestamp;

    @NotNull
    private Double amount;
}
