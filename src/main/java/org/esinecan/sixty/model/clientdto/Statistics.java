package org.esinecan.sixty.model.clientdto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Simple Pojo. Preferred over map for the sole purpose of meeting the specs
 * Created by eren.sinecan on 20/04/2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class Statistics {

    private Double sum;
    private Double avg;
    private Double max;
    private Double min;
    private Long count;
}
