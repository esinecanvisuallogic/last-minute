package org.esinecan.sixty.service.contract;

import org.esinecan.sixty.model.entitydto.DummyTransObj;
import org.springframework.http.HttpStatus;

/**
 * This is our contract. A well known principle, but let us repeat
 * out of respect for our ancestors:
 * If we were to implement an actual transaction, we
 * would just replace the class that implements this interface.
 * Created by eren.sinecan on 20/04/2017.
 */
public interface BusinessService {
    HttpStatus doTransaction(DummyTransObj object);
}
