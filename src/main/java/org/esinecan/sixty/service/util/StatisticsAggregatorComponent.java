package org.esinecan.sixty.service.util;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Timer;
import java.util.TimerTask;

import static org.esinecan.sixty.service.util.ReactiveComponent.cleanStack;

/**
 * Created by eren.sinecan on 20/04/2017.
 */
@Component
public class StatisticsAggregatorComponent {

    /**
     * This is actually bit of an antipattern. Ordinarily, we would deal with
     * expired requests in a lazy fashion. As in, we would address them when a
     * value that is affected by them is called upon. However, that would involve
     * us traversing the queue. Even if we kept the queue sorted by the timestamp,
     * and did binary search on it, we'd still be looking at a O(log n) complexity.
     *
     * But our requirements demand us to have a constant complexity here. Therefore
     * we sacrifice some preemptive memory in order to meet the requirements and
     * update the queue every second.
     * @throws InterruptedException
     */
    @PostConstruct
    public void keepStatsUpToDate() throws InterruptedException {
        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                cleanStack(false);
            }
        };
        timer.schedule(timerTask, Long.valueOf(0L), Long.valueOf(1000L));
    }
}
