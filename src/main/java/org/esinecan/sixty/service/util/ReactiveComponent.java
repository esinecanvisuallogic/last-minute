package org.esinecan.sixty.service.util;

import io.reactivex.Single;
import org.esinecan.sixty.model.entitydto.DummyTransObj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicLong;

/**
 * This is the class where we abuse RxJava library in order to be
 * nonblocking while updating our thread safe data structures.
 * While we log the error, we do not do anything to actually
 * preserve the potentially corrupt data as this is a statistical
 * business case and the heavier the load the less important
 * each individual data point will be.
 * Created by eren.sinecan on 20/04/2017.
 */
@Component
public class ReactiveComponent {
    //We use maps because O(1) access time
    public static Map<String,Double> CURRENT_STATISTICS = new ConcurrentHashMap<>();
    static {
        CURRENT_STATISTICS.put("sum", 0.0);
        CURRENT_STATISTICS.put("avg", 0.0);
        CURRENT_STATISTICS.put("max", 0.0);
        CURRENT_STATISTICS.put("min", 0.0);
        CURRENT_STATISTICS.put("count", 0.0);
    }
    public static AtomicLong NEXT_REQUEST_EXPIRATION_TIME = new AtomicLong(System.currentTimeMillis());

    public static final Queue<DummyTransObj> REQUESTS_ARRIVED_IN_LAST_60_SECONDS = new ConcurrentLinkedQueue();

    private static final Logger logger = LoggerFactory.getLogger(ReactiveComponent.class);

    /**
     * This is the method that is called by the service which is accessed by
     * the controller. Our timer task's entry point is not this method as
     * this method aims to REQUESTS_ARRIVED_IN_LAST_60_SECONDS the data unless it's too old to qualify
     * for our statistics, and timer event does not count as a user input.
     * @param objectOptional
     */
    public void saveAndUpdateStats(Optional<DummyTransObj> objectOptional){
        Single.create(singleEmitter -> {
            Thread thread = new Thread(() -> {
                try {
                    updateStats(objectOptional);
                    logger.debug("Transaction completed for object: " + objectOptional);
                }catch (Exception e){
                    singleEmitter.onError(e);
                }
            });
            thread.start();
        }).doOnError(throwable -> {
            logger.error("Transaction failed for object: " + objectOptional + " for the following reason: " + throwable);
        }).subscribe().dispose();
    }

    /**
     * This method is what resides inside our rudimentary concurrent call. It
     * starts out by adding the request at hand if not too stale, cleans out the
     * stale ones that are already in queue, and recalculates the CURRENT_STATISTICS for the last
     * 60 seconds.
     * @param objectOptional
     */
    public static void updateStats(Optional<DummyTransObj> objectOptional){
        long minTime = System.currentTimeMillis() - 60000;
        if(objectOptional.isPresent() && objectOptional.get().getTimestamp() > minTime){
            REQUESTS_ARRIVED_IN_LAST_60_SECONDS.add(objectOptional.get());
            cleanStack(true);
        }else{
            cleanStack(false);
        }
    }

    /**
     * This method utilizes the ConcurrentLinkedQueue's parent Queue's
     * removeIf. This method has linear, aka O(n) complexity and
     * brings a good advantage over Iterator's quadratic, aka O(n^2)
     * complexiy. Once we removed the stale elements of necessary,
     * we pass the queue to recalculation step.
     * @param added
     */
    public static void cleanStack(boolean added) {
        try{
            long minTime = System.currentTimeMillis() - 60000;
            if(NEXT_REQUEST_EXPIRATION_TIME.get() < minTime && REQUESTS_ARRIVED_IN_LAST_60_SECONDS.size() > 0){
                REQUESTS_ARRIVED_IN_LAST_60_SECONDS.removeIf(dummyTransObj -> dummyTransObj.getTimestamp() < minTime);
                recalculate(added, true);
            }else {
                recalculate(added, false);
            }
            NEXT_REQUEST_EXPIRATION_TIME.set((REQUESTS_ARRIVED_IN_LAST_60_SECONDS.peek() != null) ? (REQUESTS_ARRIVED_IN_LAST_60_SECONDS.peek().getTimestamp() + 60000) : (System.currentTimeMillis() + 60000));
        }catch (Exception e){
            logger.error(e.getMessage());
        }
    }

    /**
     * This is the method that uses java streams to REQUESTS_ARRIVED_IN_LAST_60_SECONDS the values into our concurrent
     * Hashmap. We do not prefer parallelstreams here as they will clog up all of the
     * ForkJoinPools as the input size increases.
     * @param added
     * @param removed
     */
    private static void recalculate(boolean added, boolean removed) {
        if (added || removed) {
            CURRENT_STATISTICS.replace("sum",
                    REQUESTS_ARRIVED_IN_LAST_60_SECONDS.stream().mapToDouble(DummyTransObj::getAmount).sum());
            CURRENT_STATISTICS.replace("avg",
                    REQUESTS_ARRIVED_IN_LAST_60_SECONDS.stream().mapToDouble(
                            DummyTransObj::getAmount).average().getAsDouble());
            CURRENT_STATISTICS.replace("max",
                    REQUESTS_ARRIVED_IN_LAST_60_SECONDS.stream().mapToDouble(
                            DummyTransObj::getAmount).max().getAsDouble());
            CURRENT_STATISTICS.replace("min",
                    REQUESTS_ARRIVED_IN_LAST_60_SECONDS.stream().mapToDouble(
                            DummyTransObj::getAmount).min().getAsDouble());
            CURRENT_STATISTICS.replace("count",
                    Double.valueOf(REQUESTS_ARRIVED_IN_LAST_60_SECONDS.stream().mapToDouble(
                            DummyTransObj::getAmount).count()));
        }
    }
}
