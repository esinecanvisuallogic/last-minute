package org.esinecan.sixty.service.implementation;

import org.esinecan.sixty.model.entitydto.DummyTransObj;
import org.esinecan.sixty.service.contract.BusinessService;
import org.esinecan.sixty.service.util.ReactiveComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

/**
 * This is our service implementation that prepares our data on a separate thread.
 *
 * If there were any db operations, we'd use transaction annotation coupled with
 * JDBC Data source to protect our connection pool from being exhausted as it is
 * under a high load of concurrent requests.
 *
 * Created by eren.sinecan on 20/04/2017.
 */
@Service
public class BusinessServiceImpl implements BusinessService{
    @Autowired
    private ReactiveComponent reactiveComponent;

    @Override
    public HttpStatus doTransaction(DummyTransObj object) {
        reactiveComponent.saveAndUpdateStats(Optional.of(object));
        int randomNum = ThreadLocalRandom.current().nextInt(0, 2 + 1);
        return (randomNum > 0) ? HttpStatus.CREATED : HttpStatus.NO_CONTENT; //Completely arbitrary decision here.
    }
}
