package org.esinecan.sixty.web;

import org.esinecan.sixty.model.clientdto.Statistics;
import org.esinecan.sixty.model.entitydto.DummyTransObj;
import org.esinecan.sixty.service.contract.BusinessService;
import org.esinecan.sixty.service.util.StatisticsAggregatorComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.esinecan.sixty.service.util.ReactiveComponent.CURRENT_STATISTICS;

/**
 * Created by eren.sinecan on 20/04/2017.
 */
@RestController
public class ImportantBusinessController {

    @Autowired
    private BusinessService service;

    @Autowired
    private StatisticsAggregatorComponent aggregateRunner;

    /**
     * A minimal controller that fits the specs
     * @param object has timestamp and amount
     * @return 201 or 204 with empty body
     */
    @RequestMapping(value = "/transactions", method = RequestMethod.POST)
    public ResponseEntity transactions(@RequestBody @Valid DummyTransObj object){
        return new ResponseEntity(service.doTransaction(object));
    }

    @RequestMapping(value = "/statistics", method = RequestMethod.GET)
    public Statistics statistics(){
        Statistics statistics = new Statistics();
        statistics.setAvg(CURRENT_STATISTICS.get("avg"));
        statistics.setCount(CURRENT_STATISTICS.get("count").longValue());
        statistics.setMax(CURRENT_STATISTICS.get("max"));
        statistics.setMin(CURRENT_STATISTICS.get("min"));
        statistics.setSum(CURRENT_STATISTICS.get("sum"));

        return statistics;
    }
}
