angular.module('clientapp', ['jsonFormatter']).config(function($httpProvider) {

    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

}).controller('home',

    function($http, $rootScope, $interval, $timeout, JSONFormatterConfig) {

        var self = this;

        self.running = false;

        self.timeouty = {};

        self.intervalms = 1000;

        self.percentreq = 50;

        self.Math = window.Math;

        self.messages = [];

        JSONFormatterConfig.hoverPreviewEnabled = false;

        self.initialization = function(){
            $http.get('base').then(function(response) {
                self.baseUrl = response.toString();
            });
        };

        self.initialization();

        self.startStop = function () {
            self.running = !self.running;

            if(self.running){
                self.timeouty = $interval(self.attemptRequest, self.intervalms);
            }else{
                $interval.cancel(self.timeouty);
                self.collectData();
            }
        };

        self.attemptRequest = function () {
            var diceroll = self.Math.floor(self.Math.random() * 100);
            if(diceroll >= self.percentreq){
                var reqObj = {};
                reqObj.amount = self.Math.random() * 100;
                var reqDate = new Date();
                reqObj.timestamp = reqDate.getTime();
                $http.post('/transactions', reqObj).then(function(response) {
                    reqObj.formattedDate = reqDate.toTimeString();
                    self.messages.push(reqObj);
                }, function(errorResponse){
                    self.showErrorMsg = true;
                    self.errorMsg = errorResponse.data.message;
                    $interval(function(){
                        self.showErrorMsg = false;
                        self.errorMsg = "";
                    }, self.intervalms);
                });
            }
        };

        self.collectData = function () {
            $http.get('/statistics').then(function(response) {
                self.serverResp = response.data;
                var stopTime = (new Date()).getTime();
                var newMsg = [];
                self.messages.forEach(function (message) {
                    if(stopTime - message.timestamp <= 60000){
                        newMsg.push(message);
                    }
                });
                self.messages = newMsg;
            });
        };
    });