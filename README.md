# Last minute request stats #

Assuming we have an endpoint under heavy load, this application collects the values from incoming requests and calculates their average, sum, min, max and count in a thread safe manner.

This very small application that is made of Spring-boot and AngularJS for a testing GUi.

### Set-up ###

i've committed everything. You don't even need to do a mvn package. Just pull the project and *mvn spring-boot:run* the Application class, go to http://localhost:8080 and test.